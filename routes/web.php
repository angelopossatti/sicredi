<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Controller de Clientes acessível somente para usuário autenticado
Route::middleware(['auth'])->group(function(){
	Route::get('/clientes/monitor', 'ClientsController@getMonitorData');
	Route::get('/monitor', 'ClientsController@monitor');
	Route::get('/auditoria/relatorio', 'ActionAuditsController@report');

	Route::resource('clientes', 'ClientsController');
	Route::resource('formas_contato', 'ContactMeansController');
	Route::resource('auditoria', 'ActionAuditsController');
});