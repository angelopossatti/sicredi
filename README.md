<p>1- Criei projeto Composer [version 1.6.5]: Versão do Laravel 5.5.*</p> 
<p>2- Utilizei PHP 7.0.30 </p>
<p>3- No arquivo .env o login e senha do mysql estão como root | root</p>
<p>4- Utilizei Bootsrap, Javascript, jQuery e Vue.js </p>
<p>5- Para comunicação entre componentes Vue.js utilizei o VueX </p>
<p>6- Necessidade de executar o projeto com acesso a internet, pois utilizei ícones do Ionic, através de uma referêcia externa CDN.</p>
<p>7- Outra referência CDN foi de estilo, precisei para o relatório de auditoria de ações (gerar PDF). Infelizmente em alguns momentos ele gera o PDF sem estilo da tabela, porém na grande maioria das vezes um F5 resolveu.</p>

<p>Para executar o projeto, recomenda-se que seja feito o clone através do link https://gitlab.com/angelopossatti/sicredi.git</p>

<p>Posteriormente configurar a base de dados do MYSQL no arquivo .env (DB_DATABASE
DB_USERNAME,DB_PASSWORD). O nome da base de dados está conforme sugerido pelo Sicredi (db_prova_02456968069)</p>

<p>Executar as migrações para geração das tabelas</p>

<p>Após essas etapas, executar o servidor do laravel na pasta do projeto: php artisan serve </p>

<p>Acessar http://localhost:8000 (ou porta criada)</p>