<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\ContactMean;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $totalClients = Client::count();
        $totalContactMeans = ContactMean::count();

        return view('home', compact('totalClients','totalContactMeans'));
    }
}
