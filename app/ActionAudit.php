<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActionAudit extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id','user_login','date','local','action','registry_id'];
    protected $dates = ['deleted_at'];
}
