@extends('layouts.app')

@section('content')
    <page-component>
        @if($errors->all())
            <div style="" class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                @if($errors->all())   
                    @foreach ($errors->all() as $value)
                        <li>{{ $value }}</li>
                    @endforeach
                @endif
            </div>
        @endif
        
        <panel-component headline="Formas de Contato">
        	<table-component
         		v-bind:fields="['ID','Título']"
        		v-bind:items="{{ $data }}"
                read = "/formas_contato/"
                update = "/formas_contato/"
                remove = "/formas_contato/"
        		token="{{ csrf_token() }}">
        	</table-component>
        </panel-component>
    </page-component>

    {{-- Modal para adicionar nova forma de contato --}}
    <modal-component id="toCreate" title="Novo">
        <form-component 
            id="form-create"
            action="{{ route('formas_contato.store') }}" 
            method="POST"
            enctype=""
            token="{{ csrf_token() }}">

            {{-- Forma de Contato --}}
            <div class="form-group">
                <label for="title">Tipo de Contato</label>
                <input type="text" required="true" class="form-control" id="title" name="title" placeholder="Ex: Presencial" value="{{ old('title') }}">
            </div>
        </form-component>

        {{-- Botões --}}
        <span slot="buttons">
            <button form="form-create" class="btn btn-info">Salvar</button>
        </span>
    </modal-component>

    {{-- Modal para editar forma de contato --}}
    <modal-component id="toUpdate" title="Editar">
        <form-component 
            id="form-edit"
            v-bind:action="'/formas_contato/' + $store.state.item.id" 
            method="PUT"
            enctype="multipart/form-data" 
            token="{{ csrf_token() }}">

            {{-- forma de contato --}}
            <div class="form-group">
                <label for="title">Tipo de Contato</label>
                <input type="text" required="true" class="form-control" id="title" name="title" v-model="$store.state.item.title">
            </div>
        </form-component>

        {{-- Botões --}}
        <span slot="buttons">
            <button form="form-edit" class="btn btn-info">Salvar</button>
        </span>
    </modal-component>

    {{-- Modal para visualizar detalhes da forma de contato --}}
    <modal-component id="toRead" title="Detalhes">
        {{-- Forma de Contato --}}
        <div class="form-group">
            <label for="title">Tipo de Contato: </label> 
            @{{ $store.state.item.title }}
        </div>
    </modal-component>

@endsection
